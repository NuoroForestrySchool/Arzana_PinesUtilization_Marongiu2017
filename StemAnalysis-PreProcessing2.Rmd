---
title: "Stem analysis - preprocessing"
subtitle: "Just first checks"
author:
- name: Roberto Scotti
  affiliation: NuoroForestrySchool
abstract: |
  Preprocessing includes all steps from accessing raw input on GoogleSheets to the production of a validated SQLite DB.  
  This version stops processing after a first check on input data.  
  Repo: https://gitlab.com/NuoroForestrySchool/Arzana_PinesUtilization_Marongiu2017
keywords: "data wrangling"
date: "`r format(Sys.time(), '%B %d, %Y')`"

output: html_notebook
# YAML from http://svmiller.com/blog/2016/02/svm-r-markdown-manuscript/
---

Data tally procedures and forms are presented in this document:
[PROTOCOLLO DI RILIEVO CANALI ESCA_V2.pdf](https://drive.google.com/drive/folders/0Bw43-keDfJfFWlVETDlDWmtVeEk)

```{r AccessGS}
library(tidyverse)
library(googlesheets)
suppressMessages(library(dplyr))

# URL del tabellone https://docs.google.com/spreadsheets/d/1Wh0gII8TdrOAcPP4iOK3idmK_BEcZF_35xsoaQobTuU/edit#gid=607028096
input.gs <- 
  "FormsInserimentoDati_Arzana2018_2"  %T>%
  gs_ls %>%
  gs_title

input.gs %>%
  gs_ws_ls

for(s in c("Intestazione", "Scheda_1", "Scheda_2", "Rotelle"  )) {
  assign(s ,   gs_read(input.gs, ws = s))
}

Intestazione <- Intestazione %>% 
  fill(complesso)
Scheda_1 <- Scheda_1 %>% 
  fill(id_fusto_campione, progressivo_foglio) %>%
  mutate(distanza_suolo = 
           parse_double(distanza_suolo,
                        locale = locale(decimal_mark = ",")))
Scheda_2 <- Scheda_2 %>% 
  fill(id_fusto_campione, progressivo_foglio, id_asta_secondaria)

```

# Basic statistics

```{r}
library(magrittr)
numbers_only <- function(x) !grepl("\\D", x)

cat("Foglio: 'Intestazione'\n")
Intestazione %T>%
  {print(paste("N. di fusti campione - Totale (acquisito):", length(unique(Intestazione$id_fusto_campione))))} %>%
  group_by(id_gradone, specie) %>% 
  summarise( n_FC = n()) %>% 
  spread(specie, n_FC) %>% 
  arrange(ifelse(numbers_only(.$id_gradone), str_sub(paste0("000", .$id_gradone), -3), .$id_gradone)) %>% hux(add_colnames = T)
Intestazione %>%
  group_by(specie) %>%
  summarise(n_FC = n(), min_dbh = min(.$d_130), max_dbh = max(.$d_130)) %>%
  hux(add_colnames = T)

```



# Distances from ground should be stricly increasing

```{r check1}
library(huxtable)

Scheda_1 %>%
  rowid_to_column %>%
  group_by(id_fusto_campione) %>%
  mutate(dist_succ = lead(distanza_suolo, 1), tree_row = row_number()) %>%
  select(1:3, distanza_suolo, tree_row, dist_succ) %>%
  filter(tree_row >3 & distanza_suolo >= dist_succ) %>%
  hux(add_colnames = TRUE) %>%
  set_align( every(), 1:3, 'center')  %>%
  set_caption('Rows after wich distance decreases') %>%
  theme_plain()

cat(paste("Analysis completed at:", Sys.time()))

```


```{r echo=FALSE}
# rmarkdown::render("StemAnalysis-PreProcessing.Rmd")
# markdown::rpubsUpload("Stem analysis - data wrangling", "StemAnalysis-PreProcessing.nb.html")
```

