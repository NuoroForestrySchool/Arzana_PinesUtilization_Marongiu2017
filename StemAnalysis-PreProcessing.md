---
title: "Stem analysis - preprocessing"
thanks: "Repo: https://gitlab.com/NuoroForestrySchool/Arzana_PinesUtilization_Marongiu2017"
author:
- name: Roberto Scotti
  affiliation: NuoroForestrySchool
abstract: "From raw input on GoogleSheets to the SQLite DB"
keywords: "data wrangling"
date: "agosto 31, 2018"

output: html_notebook

---

Data tally procedures and forms are presented in this document
[PROTOCOLLO DI RILIEVO CANALI ESCA_V2.pdf](https://drive.google.com/drive/folders/0Bw43-keDfJfFWlVETDlDWmtVeEk)


```r
# YAML from http://svmiller.com/blog/2016/02/svm-r-markdown-manuscript/
library(tidyverse)
```

```
## -- Attaching packages ---------------------------------- tidyverse 1.2.1 --
```

```
## v ggplot2 3.0.0     v purrr   0.2.5
## v tibble  1.4.2     v dplyr   0.7.6
## v tidyr   0.8.1     v stringr 1.3.1
## v readr   1.1.1     v forcats 0.3.0
```

```
## -- Conflicts ------------------------------------- tidyverse_conflicts() --
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

```r
library(googlesheets)
suppressMessages(library(dplyr))

# URL del tabellone https://docs.google.com/spreadsheets/d/1Wh0gII8TdrOAcPP4iOK3idmK_BEcZF_35xsoaQobTuU/edit#gid=607028096
input.gs <- 
  "FormsInserimentoDati_Arzana2018_2"  %T>%
  gs_ls %>%
  gs_title
```

```
## Sheet successfully identified: "FormsInserimentoDati_Arzana2018_2"
```

```r
input.gs %>%
  gs_ws_ls
```

```
## [1] "ISSUES"       "Intestazione" "Scheda_1"     "Scheda_2"    
## [5] "Rotelle"
```

```r
for(s in c("Intestazione", "Scheda_1", "Scheda_2", "Rotelle"  )) {
  assign(s ,   gs_read(input.gs, ws = s))
}
```

```
## Accessing worksheet titled 'Intestazione'.
```

```
## Parsed with column specification:
## cols(
##   complesso = col_character(),
##   data = col_character(),
##   rilevatori = col_character(),
##   id_gradone = col_character(),
##   progr_gradone = col_character(),
##   id_fusto_campione = col_integer(),
##   specie = col_character(),
##   d_130 = col_integer(),
##   h_ipso = col_number(),
##   lung_atterrato = col_number(),
##   peso_ramaglia = col_integer(),
##   fascine = col_character()
## )
```

```
## Accessing worksheet titled 'Scheda_1'.
```

```
## Parsed with column specification:
## cols(
##   id_fusto_campione = col_integer(),
##   progressivo_foglio = col_integer(),
##   distanza_suolo = col_character(),
##   diam_sezione = col_integer(),
##   id_asta_secondaria = col_character(),
##   stato_palco = col_character(),
##   numero_rami = col_character(),
##   diam_ramo_grosso = col_integer()
## )
```

```
## Accessing worksheet titled 'Scheda_2'.
```

```
## Parsed with column specification:
## cols(
##   id_fusto_campione = col_integer(),
##   progressivo_foglio = col_integer(),
##   id_asta_secondaria = col_character(),
##   dist_da_biforcazione = col_character(),
##   diam_sezione = col_character()
## )
```

```
## Accessing worksheet titled 'Rotelle'.
```

```
## Parsed with column specification:
## cols(
##   id_fusto_campione = col_integer(),
##   peso_rotelle = col_number(),
##   dist_rot_id0 = col_character(),
##   dist_rot_id1 = col_character(),
##   dist_rot_id2 = col_number(),
##   dist_rot_id3 = col_number(),
##   dist_rot_id4 = col_number(),
##   dist_rot_id5 = col_number(),
##   dist_rot_id6 = col_number()
## )
```

```r
Intestazione <- Intestazione %>% 
  fill(complesso)
Scheda_1 <- Scheda_1 %>% 
  fill(id_fusto_campione, progressivo_foglio) %>%
  mutate(distanza_suolo = 
           parse_double(distanza_suolo,
                        locale = locale(decimal_mark = ",")))
Scheda_2 <- Scheda_2 %>% 
  fill(id_fusto_campione, progressivo_foglio, id_asta_secondaria)
```

# Distances from ground should be stricly increasing
Figures in the lines after the ollowing present decresaing distances

```r
Scheda_1 %>%
  rowid_to_column %>%
  group_by(id_fusto_campione) %>%
  mutate(d2 = lead(distanza_suolo, 1), tree_row = row_number()) %>%
  select(1:3, distanza_suolo, d2, tree_row) %>%
  filter(tree_row >3 & distanza_suolo >= d2) %>%
  print(pillar.sigfig = 5, n = Inf)
```

```
## # A tibble: 34 x 6
## # Groups:   id_fusto_campione [23]
##    rowid id_fusto_campione progressivo_foglio distanza_suolo    d2 tree_row
##    <int>             <int>              <int>          <dbl> <dbl>    <int>
##  1    67                 4                  2          11.0  10.8        67
##  2   172                 6                  1           8.35  7.5        36
##  3   336                12                  2          10.6  10.4        70
##  4   387                14                  1           9.3   7.5        34
##  5   429                16                  1           7.3   4.45       27
##  6   758                33                  2           8.55  7.65       50
##  7   927                39                  1           5.65  5.56       28
##  8  1341                50                  2          11.4   1.55       50
##  9  1354                50                  2          16.5  14.8        63
## 10  1421                51                  1         310.   10.6        41
## 11  1546                54                  2          19.8  19.0        55
## 12  1812                66                  1          40.6  10.8        44
## 13  1843                66                  2          15.8  15.0        75
## 14  1911                67                  1           5.6   5.55       16
## 15  1933                67                  1          15.3  15.1        38
## 16  1980                68                  1           3.85  1          19
## 17  2034                68                  2          18.2  14.4        73
## 18  2039                68                  2          19.0  18.2        78
## 19  2047                68                  2          20.2  20.0        86
## 20  2296                72                  1          15.4  15.2        38
## 21  2352                72                  3          26.4  26.3        94
## 22  2436                73                  2          19.4  16.6        65
## 23  2476                73                  3          25    24.1       105
## 24  2478                73                  3          25.2  24.3       107
## 25  2525                77                  1           9.75  9.5        31
## 26  2813                 3                  1           2.6   1.75        7
## 27  2828                 3                  1           4.9   2.1        22
## 28  2830                 3                  1           5.25  4.5        24
## 29  3091                10                  1           9.1   3.3        11
## 30  3140                13                  1           7.75  5.55       27
## 31  3243                15                  2          11     1.15       57
## 32  3269                17                  1           6.35  5.5        25
## 33  3385                19                  1           6.3   5.55       29
## 34  3522                21                  1           7.1   7.1        28
```



```r
# rmarkdown::render("StemAnalysis-PreProcessing.Rmd")
```

